package model;

import java.util.UUID;

public class Category extends Base {
    public Category(String name, UUID parentId) {
        super.name = name;
        super.parentId = parentId;
    }
}
