package service;

import model.Category;
import model.Product;

import java.util.UUID;

public class ProductService extends BaseService {
    private Product[] products = new Product[1000];
    private int indexProducts;

    public Product[] getProducts() {
        return products;
    }

    @Override
    public boolean add(Object object) {
        Product product = (Product) object;

        if (!validate(product)) {
            return false;
        }

        products[indexProducts++] = product;

        return true;
    }

    @Override
    public boolean delete(UUID id) {
        Product deleteProduct = getProductById(id);

        if (deleteProduct == null) {
            return false;
        }

        int deleteIndex = getIndexProduct(deleteProduct);

        if (deleteIndex == -1) {
            return false;
        }

        for (int i = deleteIndex; i < indexProducts; i++) {
            if (products[i] != null) {
                products[i] = products[i++];
            }
        }

        return true;
    }

    public Product getProductById(UUID id) {
        for (Product product : products) {
            if (product != null && product.getId().equals(id)) {
                return product;
            }
        }

        return null;
    }

    public Product getProductByName(String name) {
        for (Product product : products) {
            if (product != null && product.getName().equals(name)) {
                return product;
            }
        }

        return null;
    }

    @Override
    public Object[] list(UUID id) {
        Product[] parentProducts = new Product[100];
        int index = 0;

        return parentProducts;
    }

    private int getIndexProduct(Product product) {
        if (product == null) {
            return -1;
        }

        for (int i = 0; i < indexProducts; i++) {
            if (products[i].getName().equals(product.getName())) {
                return i;
            }
        }

        return -1;
    }


    private boolean validate(Object object) {
        Product product = (Product) object;

        return (product != null && !isExist(product.getName()));
    }

    private boolean isExist(String name) {
        for (Product product : products) {
            if (product != null && product.getName().equals(name)) {
                return true;

            }
        }

        return false;
    }


}
